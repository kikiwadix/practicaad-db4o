/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendaanimalesdb4opractica;

import java.util.ArrayList;

/**
 *
 * @author JuanPablo
 */
public class Animal {
    private int id;
    private String especie;
    private String color;
    
    private boolean visible;
    
    private ArrayList<Long> revisiones;

    public Animal(int id, String especie, String color, ArrayList<Long> aux) {
        this.id = id;
        this.especie = especie;
        this.color = color;
        
        this.revisiones = aux;
        
        this.visible = true;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public long getRevisiones(int pos){
        return revisiones.get(pos);
    }
    
    public ArrayList<Long> getRevisiones() {
        return revisiones;
    }

    public void setRevisiones(ArrayList<Long> revisiones) {
        this.revisiones = revisiones;
    }
    
    public void setRevisiones(long valor){
        this.revisiones.add(valor);
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
    
    
    
}
