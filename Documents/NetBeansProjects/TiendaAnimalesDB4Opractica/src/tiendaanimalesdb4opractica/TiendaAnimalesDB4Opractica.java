/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendaanimalesdb4opractica;

import com.db4o.Db4oEmbedded;
import com.db4o.ObjectContainer;
import com.db4o.ObjectSet;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;



/**
 *
 * @author JuanPablo
 */
public class TiendaAnimalesDB4Opractica {
    
    //aki indicamos donde está el archivo base de datos
    final static String BDANIMALES = "C:\\temp\\BDTiendaAnimales.yap";

    public static void main(String[] args) throws IOException{
        
        
        //variables
        Scanner sc = new Scanner(System.in);
        
        int opcion = 0;
        boolean salir = true, encontrado = false;
        
        String dni, nombre, email;
        Long telf;
        
        String especie, color;
        int id;
        
        String nombreVet, apellidos;
        Long colegiado;
        
        String cadenaCompras = "";
        
        String aux = "";
        
        int clienteCompra = 0, animalCompra = 0, animalRevision = 0;
        long veterinarioRevision;
        boolean aux2 = false;
        
        int clienteModificar = -1, animalModificar = -1, veterinarioModificar = -1;
        
        
        
        //LOS ARRAYS
       
        //un array de compras vacio
        ArrayList<Integer> comprasVacio = new ArrayList<>();
        //un array de revisiones en veterinario vacio
        ArrayList<Long> revisionesVacio = new ArrayList<>();
        //array aux, para cuando modifico clientes
        ArrayList<Integer> arrayAux = new ArrayList<>();
        //array aux para cuando modifico animales
        ArrayList<Long> arrayAuxLong = new ArrayList<>();
        
        
        //creo los 3 arrays para guardar la informacion
        ArrayList<Cliente> clientes =  new ArrayList<>();
        ArrayList<Animal> animales = new ArrayList<>();
        ArrayList<Veterinario> veterinarios = new ArrayList<>();
        
       
        //abrimos la base de datos, creo el bjeto db aki, y cuando abro la base de datos luego utilizo el mismo
        ObjectContainer db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), BDANIMALES);
        
        
        
        //al principio siempre leo los elementos, y los meto en sus arrays
        clientes = LeerElementosCliente(db);
        animales = LeerElementosAnimal(db);
        veterinarios = LeerElementosVeterinario(db);
        
        //y cierro la base de datos
        //db.close();
        
        
      
        /*
        
        //CREACION A MANO DE OBJETOS EN EL ARCHIVO
        
        //creamos animales
        Animal a1 = new Animal(1, "Gato", "Blanco", revisionesVacio);
        Animal a2 = new Animal(2, "Agaporni", "Verde", revisionesVacio);
        Animal a3 = new Animal(3, "Perro", "Marron", revisionesVacio);
        Animal a4 = new Animal(4, "Hamster", "Gris", revisionesVacio);
        
        //almacenar objetos animal en la base de datos
        db.store(a1);
        db.store(a2);
        db.store(a3);
        db.store(a4);
        
        //creamos clientes
        Cliente c1 = new Cliente("75132205Z","Juan Pablo Sanchez Salmeron", "waykiki01@gmail.com", 679772752, comprasVacio);
        Cliente c2 = new Cliente("11223344F","Federico Dávila Almodovar", "fedeee@gmail.com", 645767678, comprasVacio);
        //almacenar objetos cliente en la base de datos
        db.store(c1);
        db.store(c2);
        
        //creamos veterinarios
        Veterinario v1 = new Veterinario(223344, "Anselmo", "De la Rosa Del Arbol");
        Veterinario v2 = new Veterinario(556677, "Antoine", "Jimenez Griezmann");
        //almacenar objetos veterinario en la base de datos
        db.store(v1);
        db.store(v2);
        
        
        */
        
        
        
        //menu
        System.out.println("\nHola Usuario, bienvenido al software de la tienda +COTAS! ");
        do{
            salir = true;
            System.out.println("\nQue desea realizar?");
            System.out.println("\t1 para ver LISTA de CLIENTES y sus compras");
            System.out.println("\t2 para insertar/modificar/eliminar cliente\n");
            System.out.println("\t3 para ver LISTA de ANIMALES y sus visitas al veterinario");
            System.out.println("\t4 para insertar/modificar/eliminar animal\n");
            System.out.println("\t5 para REALIZAR una COMPRA\n");
            System.out.println("\t6 para ver LISTA de VETERINARIOS");
            System.out.println("\t7 para insertar/modificar/eliminar veterinario\n");
            System.out.println("\t8 para LLEVAR un animal al VETERINARIO\n");
            System.out.println("\t9 para GUARDAR y SALIR");
            opcion = sc.nextInt();
            
            switch(opcion){
                
                case 1:
                    for(int i=0;i<clientes.size();i++){
            
                        if(clientes.get(i).isVisible() == true){
                            
                            System.out.println();
                            System.out.println("Cliente nº " + (i+1) );
                            System.out.println("DNI: " + clientes.get(i).getDni() );
                            System.out.println("Nombre: " + clientes.get(i).getNombre() );
                            System.out.println("E-mail: " + clientes.get(i).getEmail() );
                            System.out.println("Teléfono: " + clientes.get(i).getTelf() );

                            if(clientes.get(i).getCompras().size() > 0){
                                System.out.println("\n\tCompras: ");
                                for(int j=0; j<clientes.get(i).getCompras().size() ; j++){

                                        System.out.println();
                                        System.out.println("\n\tId: " + animales.get( clientes.get(i).getCompras(j) -1 ).getId() );
                                        System.out.println("\tEspecie: " + animales.get( clientes.get(i).getCompras(j) -1 ).getEspecie() );
                                        System.out.println("\tColor: " + animales.get( clientes.get(i).getCompras(j) -1 ).getColor() ); 


                                }
                            }
                            System.out.println("______________________");
                        }
                        
                    }
                    break;
                    
                case 2:
                    
                    System.out.println("\nQue desea realizar?\nPulse 1 para insertar nuevo\nPulse 2 para modificar un cliente\nPulse 3 para eliminar");
                    opcion = sc.nextInt();
                    
                    if(opcion == 1){
                        sc.nextLine();
                        System.out.println("Introduce el DNI del cliente: ");
                        dni = sc.nextLine();
                        System.out.println("Ahora introduce el nombre: ");
                        nombre = sc.nextLine();
                        System.out.println("Ahora introduce el email: ");
                        email = sc.nextLine();
                        System.out.println("Y el telefono: ");
                        telf = sc.nextLong();
                        clientes.add(new Cliente(dni, nombre, email, telf, comprasVacio) );
                        System.out.println("\n\tNuevo cliente añadido correctamente");
                    }
                    
                    
                    if(opcion == 2){
                        clienteModificar = -1;
                        sc.nextLine();
                        System.out.println("Introduce el DNI del cliente que desea modificar: ");
                        dni = sc.nextLine();
                        
                        for(int i=0;i<clientes.size();i++){
                            if(clientes.get(i).getDni().equals(dni)){
                                clienteModificar = i;
                            }
                        }
                        
                        if(clienteModificar != -1){
                            System.out.println("Introduce el nuevo DNI del cliente (antes " + clientes.get(clienteModificar).getDni() + "):");
                            dni = sc.nextLine();
                            System.out.println("Ahora introduce el nuevo nombre (antes " + clientes.get(clienteModificar).getNombre() + "):");
                            nombre = sc.nextLine();
                            System.out.println("Ahora introduce el nuevo email (antes " + clientes.get(clienteModificar).getEmail() + "):");
                            email = sc.nextLine();
                            System.out.println("Y el nuevo telefono (antes " + clientes.get(clienteModificar).getTelf() + "):");
                            telf = sc.nextLong();
                            
                            arrayAux = clientes.get(clienteModificar).getCompras();
                            
                            //ya tengo los nuevos datos del cliente modificados
                            //asi ke ahora voy a borrarlo de la base de datos
                            Cliente cli = new Cliente(clientes.get(clienteModificar).getDni(), clientes.get(clienteModificar).getNombre(), clientes.get(clienteModificar).getEmail(), clientes.get(clienteModificar).getTelf(), clientes.get(clienteModificar).getCompras());

                            ObjectSet<Cliente> resultCliente = db.queryByExample(cli);
                            
                            while(resultCliente.hasNext()){
                                Cliente b = resultCliente.next();
                                db.delete(b);
                            }
                            
                            
                            //y ahora ke esta borrado, meto el cliente modificado en su sitio en el array...
                            //para que luego al guardar se meta sin problemas y no haya duplicidades
                            clientes.set(clienteModificar, new Cliente(dni, nombre, email, telf, arrayAux));
                            System.out.println("\n\tCliente modificado correctamente");
                        }else{
                            System.out.println("\n\tNo se ha encontrado el cliente");
                        }
                        
                    }
                    
                    
                    
                    if(opcion == 3){
                        encontrado = false;
                        sc.nextLine();
                        System.out.println("Introduzca el DNI del cliente que desea eliminar: ");
                        dni = sc.nextLine();
                        for(int i=0; i<clientes.size(); i++){
                            if(clientes.get(i).getDni().equals(dni) ){
                                //clientes.remove(i);
                                clientes.get(i).setVisible(false);
                                System.out.println("\n\tCliente eliminado correctamente");
                                encontrado = true;
                            }
                        }
                        if(!encontrado){
                            System.out.println("\n\tCliente no encontrado");
                        }
                    }
                    
                    break;
                    
                    
                    
                case 3:
                    for(int i=0;i<animales.size();i++){
            
                        if(animales.get(i).isVisible() == true){
                            System.out.println();
                            System.out.println("Id: " + animales.get(i).getId() );
                            System.out.println("Especie: " + animales.get(i).getEspecie() );
                            System.out.println("Color: " + animales.get(i).getColor() ); 
                            
                            if( (int) animales.get(i).getRevisiones().size() > 0){
                                System.out.println("\n\tVisitó al veterinario: ");
                                for(int j=0; j<animales.get(i).getRevisiones().size() ; j++){
                                        //busqueda de veterinario por numero de colegiado, que es lo que se guarda en el array
                                        for(int k=0;k<veterinarios.size();k++){
                                            if(animales.get(i).getRevisiones(j) == veterinarios.get(k).getNumeroColegiado() ){
                                                
                                                System.out.println();
                                                System.out.println("\n\tNumero colegiado: " + veterinarios.get(k).getNumeroColegiado() );
                                                System.out.println("\tNombre: " + veterinarios.get(k).getNombre() );
                                                System.out.println("\tApellidos: " + veterinarios.get(k).getApellidos() );
                                                
                                            }
                                        }
                                        
                                        


                                }
                            }
                            
                            System.out.println("______________________");
                        }
                        
                    }
                    break;
                    
                    
                case 4:
                
                    System.out.println("\nQue desea realizar?\nPulse 1 para insertar nuevo\nPulse 2 para modificar\nPulse 3 para eliminar");
                    opcion = sc.nextInt();
                    
                    if(opcion == 1){
                        sc.nextLine();
                        System.out.println("Introduce la especie del animal: ");
                        especie = sc.nextLine();
                        System.out.println("Ahora introduce el color: ");
                        color = sc.nextLine();
                        animales.add(new Animal( animales.size() +1 , especie, color, revisionesVacio) );
                        System.out.println("\n\tNuevo animal añadido correctamente");
                    }
                    
                    if(opcion == 2){
                        sc.nextLine();
                        System.out.println("Introduce el ID del animal a modificar: ");
                        id = sc.nextInt();
                        for(int i=0;i<animales.size();i++){
                            if(id == animales.get(i).getId()){
                                sc.nextLine();
                                System.out.println("Introduce la nueva especie del animal (antes " + animales.get(i).getEspecie() + "): ");
                                especie = sc.nextLine();
                                System.out.println("Ahora introduce el nuevo color (antes " + animales.get(i).getColor() + "): ");
                                color = sc.nextLine();
                                arrayAuxLong = animales.get(i).getRevisiones();
                                
                                //ya tengo los nuevos datos del animal modificados
                                 //asi ke ahora voy a borrarlo de la base de datos
                                Animal ani = new Animal(animales.get(i).getId(), animales.get(i).getEspecie(), animales.get(i).getColor(), animales.get(i).getRevisiones());
        
                                ObjectSet<Animal> resultAnimal = db.queryByExample(ani);
                                
                                while(resultAnimal.hasNext()){
                                    Animal a = resultAnimal.next();
                                    db.delete(a);   
                                }
                                
                                 
                                 
                                //y ahora ke esta borrado, meto el cliente modificado en su sitio en el array...
                                //para que luego al guardar se meta sin problemas y no haya duplicidades
                                animales.set(id -1, new Animal( id , especie, color, arrayAuxLong) );
                                System.out.println("\n\tAnimal modificado correctamente");
                                animalModificar = i;
                            }
                        }
                        
                        if(animalModificar == -1){
                            System.out.println("\n\tNo se ha encontrado el animal");
                        }
                        
                        
                    }
                    
                    if(opcion == 3){
                        encontrado = false;
                        sc.nextLine();
                        System.out.println("Introduzca el ID del animal que desea eliminar: ");
                        id = sc.nextInt();
                        for(int i=0; i<animales.size(); i++){
                            if(animales.get(i).getId() == id ){
                                animales.get(i).setVisible(false);
                                //animales.remove(i);
                                System.out.println("\n\tAnimal eliminado correctamente");
                                encontrado = true;
                            }
                        }
                        if(!encontrado){
                            System.out.println("\n\tAnimal no encontrado");
                        }
                    }
                    break;
                    
                    
                case 5:
                    clienteCompra=-1;
                    sc.nextLine();
                    System.out.println("\n\tIntroduzca el DNI del cliente que va a realizar la compra: ");
                    dni = sc.nextLine();
                    for(int l=0;l<clientes.size();l++){
                        if(clientes.get(l).getDni().equals(dni) && clientes.get(l).isVisible() == true){
                            System.out.println("\n\tCliente " + clientes.get(l).getNombre() + " encontrado");
                            clienteCompra = l;
                            l = clientes.size() +2;//para que no entre en el bucle otra vez
                        }
                    }
                    if(clienteCompra == -1){
                        System.out.println("\n\tNo se ha encontrado el cliente");
                        
                    }else{
                        
                        System.out.println("\n\tLe mostramos el catálogo completo de animales: ");
                        for(int i=0;i<animales.size();i++){

                            if(animales.get(i).isVisible() == true){
                                System.out.println();
                                System.out.println("Id: " + animales.get(i).getId() );
                                System.out.println("Especie: " + animales.get(i).getEspecie() );
                                System.out.println("Color: " + animales.get(i).getColor() ); 
                                System.out.println("______________________");
                            }
                            
                        }
                        
                        System.out.println("\n\tQue animal desea comprar? Introduzca el ID: ");
                        animalCompra = sc.nextInt();
                        if(animalCompra <= animales.size()){
                            clientes.get(clienteCompra).setCompras(animalCompra);
                            System.out.println("\n\tCompra realizada!!");
                        }else{
                            System.out.println("\n\tAnimal no encontrado en catálogo");
                        }
                    }
                    

                    break;
                    
                    
                case 6:
                    for(int i=0;i<veterinarios.size();i++){
            
                        if(veterinarios.get(i).isVisible() == true){
                            System.out.println();
                            System.out.println("Numero colegiado: " + veterinarios.get(i).getNumeroColegiado() );
                            System.out.println("Nombre: " + veterinarios.get(i).getNombre() );
                            System.out.println("Apellidos: " + veterinarios.get(i).getApellidos() );
                            System.out.println("______________________");
                        }
                        
                    }
                    break;
                    
                    
                case 7:
                    
                    System.out.println("\nQue desea realizar?\nPulse 1 para insertar nuevo\nPulse 2 para modificar\nPulse 3 para eliminar");
                    opcion = sc.nextInt();
                    
                    if(opcion == 1){
                    
                        sc.nextLine();
                        System.out.println("Introduce el numero de Colegiado del veterinario: ");
                        colegiado = sc.nextLong();
                        sc.nextLine();
                        System.out.println("Ahora introduce el nombre: ");
                        nombre = sc.nextLine();
                        System.out.println("Ahora los apellidos: ");
                        apellidos = sc.nextLine();
                        veterinarios.add(new Veterinario(colegiado, nombre, apellidos) );
                        System.out.println("\n\tNuevo veterinario añadido correctamente");
                    }
                    
                    
                    if(opcion == 2){
                    
                        sc.nextLine();
                        System.out.println("Introduce el numero de colegiado del veterinario a modificar: ");
                        colegiado = sc.nextLong();
                        for(int i=0;i<veterinarios.size();i++){
                            if(colegiado == veterinarios.get(i).getNumeroColegiado()){
                                System.out.println("Introduce el nuevo numero de Colegiado (antes " + veterinarios.get(i).getNumeroColegiado() + "): ");
                                System.out.println("(OJO!! Si el veterinario ha revisado a algún animal, y cambias el numero de Colegiado, desaparecerá de la lista de revisiones de ese animal!)");
                                colegiado = sc.nextLong();
                                sc.nextLine();
                                System.out.println("Ahora introduce el nombre (antes " + veterinarios.get(i).getNombre() + "): ");
                                nombre = sc.nextLine();
                                System.out.println("Ahora los apellidos (antes " + veterinarios.get(i).getApellidos() + "): ");
                                apellidos = sc.nextLine();
                                
                                //ya tengo los nuevos datos del animal modificados
                                 //asi ke ahora voy a borrarlo de la base de datos
                                Veterinario vet = new Veterinario(veterinarios.get(i).getNumeroColegiado(), veterinarios.get(i).getNombre(), veterinarios.get(i).getApellidos());
        
                                ObjectSet<Veterinario> resultVeterinario = db.queryByExample(vet);
                                
                                while(resultVeterinario.hasNext()){
                                        Veterinario c = resultVeterinario.next();
                                        db.delete(c);   
                                }
                                
                                
                                //y ahora ke esta borrado, meto el cliente modificado en su sitio en el array...
                                //para que luego al guardar se meta sin problemas y no haya duplicidades
                                veterinarios.set(i, new Veterinario(colegiado, nombre, apellidos) );
                                System.out.println("\n\tVeterinario modificado correctamente");
                                veterinarioModificar = i;
                            }
                        }
                        
                        if(veterinarioModificar == -1){
                            System.out.println("\n\tNo se ha encontrado el veterinario");
                        }
                        
                    }
                    
                    
                    
                    if(opcion == 3){
                        encontrado = false;
                        sc.nextLine();
                        System.out.println("Introduzca el numero de colegiado que desea eliminar: ");
                        colegiado = sc.nextLong();
                        for(int i=0; i<veterinarios.size(); i++){
                            if(veterinarios.get(i).getNumeroColegiado() == colegiado ){
                                veterinarios.get(i).setVisible(false);
                                //veterinarios.remove(i);
                                System.out.println("\n\tVeterinario eliminado correctamente");
                                encontrado = true;
                            }
                        }
                        if(!encontrado){
                            System.out.println("\n\tVeterinario no encontrado");
                        }
                    }
                    break;
                    
                    
                case 8:
                    animalRevision=-1;
                    sc.nextLine();
                    System.out.println("\n\tIntroduzca el ID del animal que va a realizar la revision: ");
                    id = sc.nextInt();
                    for(int l=0;l<animales.size();l++){
                        if(animales.get(l).getId() == id && animales.get(l).isVisible() == true){
                            System.out.println("\n\tAnimal " + animales.get(l).getEspecie() + " encontrado");
                            animalRevision = l;
                            l = animales.size() +2;//para que no entre en el bucle otra vez
                        }
                    }
                    if(animalRevision == -1){
                        System.out.println("\n\tNo se ha encontrado el animal");
                        
                    }else{
                        
                        System.out.println("\n\tLe mostramos los veterinarios disponibles: ");
                        for(int i=0;i<veterinarios.size();i++){

                            if(veterinarios.get(i).isVisible() == true){
                                System.out.println();
                                System.out.println("Numero colegiado: " + veterinarios.get(i).getNumeroColegiado() );
                                System.out.println("Nombre: " + veterinarios.get(i).getNombre() );
                                System.out.println("Apellidos: " + veterinarios.get(i).getApellidos() );
                                System.out.println("______________________");
                            }
                            
                        }
                        
                        aux2 = false;
                        System.out.println("\n\tQue veterinario desea? Introduzca el número de colegiado: ");
                        veterinarioRevision = sc.nextLong();
                        for(int i=0;i<veterinarios.size();i++){
                            if(veterinarioRevision == veterinarios.get(i).getNumeroColegiado()){
                                animales.get(animalRevision).setRevisiones(veterinarios.get(i).getNumeroColegiado());
                                System.out.println("\n\tRevision al animal en el veterinario realizada!!");
                                i = veterinarios.size() +2;//para que no entre en el bucle otra vez
                                aux2 = true;
                            }
                        }
                        
                        if(!aux2){
                            System.out.println("\n\tVeterinario no encontrado");
                        }
                        
                    }
                    
                    
                    break;
                    
                    
                case 9:
                    
                    //voy a guardar los 3 arrays en el .yap  y salir del programa
                    
                    //abro la base de datos
                    //db = Db4oEmbedded.openFile(Db4oEmbedded.newConfiguration(), BDANIMALES);
                    
                    //ahora aki he de modificar los objetos que se hayan modificado, y guardar los nuevo
                    //no hace falta borrar nada porque no se borra, se pone visible = false y yasta, pero siguen ahi
                    
                    //creo una isntancia de cada uno, para compararlo
                    Cliente cli;
                    Animal ani;
                    Veterinario vet;
                    
                    //guardo los clientes
                    for(int i=0;i<clientes.size();i++){
                        
                        //tengo que declarar un objeto cliente, para compararlo con lo que quiero guardar
                        cli = new Cliente(clientes.get(i).getDni(),clientes.get(i).getNombre(), clientes.get(i).getEmail(), clientes.get(i).getTelf(), clientes.get(i).getCompras());

                        ObjectSet<Cliente> resultCliente = db.queryByExample(cli);
                        if(resultCliente.isEmpty())
                            db.store(clientes.get(i));
        
                    }
                    
                    
                    
                    //guardo los animales
                    for(int i=0;i<animales.size();i++){
                        
                        //tengo que declarar un objeto animal, para compararlo con lo que quiero buscar (todo a null y me trae TODOS)
                        ani = new Animal(animales.get(i).getId(),animales.get(i).getEspecie(), animales.get(i).getColor(), animales.get(i).getRevisiones());

                        ObjectSet<Animal> resultAnimal = db.queryByExample(ani);
                        if(resultAnimal.isEmpty())
                           db.store(animales.get(i));
                        
                    }
                    
                    
                    
                    //guardo los veterinarios
                    for(int i=0;i<veterinarios.size();i++){
                        
                        //tengo que declarar un objeto veterinario, para compararlo con lo que quiero buscar (todo a null y me trae TODOS)
                        vet = new Veterinario(veterinarios.get(i).getNumeroColegiado(), veterinarios.get(i).getNombre(), veterinarios.get(i).getApellidos());

                        ObjectSet<Veterinario> resultVeterinario = db.queryByExample(vet);
                        if(resultVeterinario.isEmpty())
                            db.store(veterinarios.get(i));
                        
                    }

                    //y vuelvo a cerrar la base de datos
                    db.close();
                    
                    //y cambio la variable para salir del programa
                    salir = false;
                    break;
                    
                default: 
                    System.out.println("\n\tNo has introducido una opción válida\n");
                    break;
            }
        
        }while(salir);
  
        
    }  //fin del main
    
    
 
    

    //Leer datos del cliente desde el .yap
    static ArrayList<Cliente> LeerElementosCliente(ObjectContainer db){
        
        ArrayList <Cliente> clientesArray = new ArrayList<>();
        
        
        //tengo que declarar un objeto cliente, para compararlo con lo que quiero buscar (todo a null y me trae TODOS)
        Cliente cli = new Cliente(null, null, null, 0, null);
        
        ObjectSet<Cliente> resultCliente = db.queryByExample(cli);
        if(resultCliente.isEmpty())
            System.out.println("No existen registros de clientes...");
        else{
            while(resultCliente.hasNext()){
                Cliente b = resultCliente.next();
                clientesArray.add(b);   
            }
        }
        
        
        
        return clientesArray;
    }    
    
    
    
    //Leer datos del animal desde el .yap
    static ArrayList<Animal> LeerElementosAnimal(ObjectContainer db){
        
        ArrayList <Animal> animalesArray = new ArrayList<>();
        
        
        //tengo que declarar un objeto animal, para compararlo con lo que quiero buscar (todo a null y me trae TODOS)
        Animal ani = new Animal(0, null, null, null);
        
        ObjectSet<Animal> resultAnimal = db.queryByExample(ani);
        if(resultAnimal.isEmpty())
            System.out.println("No existen registros de animales...");
        else{
            while(resultAnimal.hasNext()){
                Animal a = resultAnimal.next();
                animalesArray.add(a);   
            }
        }
        
        
        
      
        return animalesArray;
    }    
    
    

    //Leer datos del veterinario desde el .yap
    static ArrayList<Veterinario> LeerElementosVeterinario(ObjectContainer db){
        
        ArrayList <Veterinario> veterinariosArray = new ArrayList<>();
        
        //tengo que declarar un objeto veterinario, para compararlo con lo que quiero buscar (todo a null y me trae TODOS)
        Veterinario vet = new Veterinario(0, null, null);
        
        ObjectSet<Veterinario> resultVeterinario = db.queryByExample(vet);
        if(resultVeterinario.isEmpty())
            System.out.println("No existen registros de veterinarios...");
        else{
            while(resultVeterinario.hasNext()){
                Veterinario c = resultVeterinario.next();
                veterinariosArray.add(c);   
            }
        }
        
        
      
        return veterinariosArray;
    }   
    
    
    
    
}
