/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendaanimalesdb4opractica;

import java.util.ArrayList;

/**
 *
 * @author JuanPablo
 */
public class Cliente {
    private String dni;
    private String nombre;
    private String email;
    private long telf;
    
    private boolean visible;
    
    private ArrayList<Integer> compras;

    public Cliente(String dni, String nombre, String email, long telf, ArrayList<Integer> comprasAux) {
        this.dni = dni;
        this.nombre = nombre;
        this.email = email;
        this.telf = telf;
        
        this.compras = comprasAux;
        
        visible = true;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    
    //para que devuelva el array completo, y hacer movidas
    public ArrayList<Integer> getCompras(){
        return compras;
    }
    
    //para que devuelva el numero de animal de la compra
    public Integer getCompras(int pos) {
        return compras.get(pos);
    }

    //para incrustar de primeras un array de compras a un cliente
    public void setCompras(ArrayList<Integer> compras) {
        this.compras = compras;
    }
    
    //para meter una compra en el array de compras del cliente
    public void setCompras(int valor){
        this.compras.add(valor);
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

  
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public long getTelf() {
        return telf;
    }

    public void setTelf(long telf) {
        this.telf = telf;
    }
    
    
    
    
}
