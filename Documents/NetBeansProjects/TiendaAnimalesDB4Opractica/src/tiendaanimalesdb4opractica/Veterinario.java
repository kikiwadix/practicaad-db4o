/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiendaanimalesdb4opractica;

/**
 *
 * @author JuanPablo
 */
public class Veterinario {
    private long numeroColegiado;
    private String nombre;
    private String apellidos;
    
    private boolean visible;

    public Veterinario(long numeroColegiado, String nombre, String apellidos) {
        this.numeroColegiado = numeroColegiado;
        this.nombre = nombre;
        this.apellidos = apellidos;
        
        this.visible = true;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    
    
    public long getNumeroColegiado() {
        return numeroColegiado;
    }

    public void setNumeroColegiado(long numeroColegiado) {
        this.numeroColegiado = numeroColegiado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    
    
}
